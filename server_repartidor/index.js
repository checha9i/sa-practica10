var express = require("express");
var app = express();
var axios = require("axios");
var logger = require("morgan");
var cors = require("cors");
const bodyParser = require("body-parser");

var pedidos = [];

app.listen(3001, function () {
  console.log("server escuchando en http://localhost:3001");
});
app.use(bodyParser.json());
app.use(cors());
app.use(logger("dev"));
app.post("/EntregarPedido", (req, res) => {
  var pedido = req.body;
  pedido.Estado = "En Camino";
  //asignando repartidor
  pedido.repartidor = parseInt(Math.random() * (10 - 1) + 1);
  pedidos.push(pedido);
  /**
   * Cambiar Estado de Preparado a En Camino
   */
  setTimeout(function () {
    let pedidoIndex = pedidos.findIndex((obj) => obj.id == pedido.id);
    //Update object's name property.
    pedidos[pedidoIndex].Estado = "En Camino";
    console.log("Pedido:  " + pedido.pedido + " en camino");
  }, 1000);

  /**
   * Entregar Pedido
   */
  setTimeout(() => {
    let pedidoIndex = pedidos.findIndex((obj) => obj.id == pedido.id);
    //Update object's name property.
    pedidos[pedidoIndex].Estado = "Entregado";
    console.log("Pedido:  " + pedido.pedido + ", ENTREGADO");
  }, 10000);
  res.send(pedido);
});

app.post("/VerificarEstado", (req, res) => {
  var id = req.body.id;

  console.log("Verificar Pedido:");
  console.log(pedidos);
  var pedido = pedidos.filter((pedido) => pedido.id == id);
  res.send(pedido[0]);
});
