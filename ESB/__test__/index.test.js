const request = require("supertest");
const app = require("../app");
let server, agent;

// Se inicia el servidor para comenzar a escuchar peticiones
beforeEach((done) => {
  server = app.listen(3002, (err) => {
    if (err) return done(err);
    agent = request.agent(server); // se obtiene el puerto en el que corre el server
    done();
  });
});
// despues de cada prueba se cierra el servidor iniciado en beforeEach
afterEach((done) => {
  return server && server.close(done);
});

// Server
describe("Pruebas EBS", function () {
  test("post /SolicitarPedido", function (done) {
    request(app)
      .post("/SolicitarPedido")
      .set({ "Content-Type": "application/json" })
      .send({
        pedido: "pedido1",
      })
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });
});
