import React,{useState} from 'react';
import './App.css';
import { Container, Button, Label ,Input,Form, FormGroup,Row,Col} from "reactstrap"
import axios from 'axios'

import { UncontrolledAlert } from 'reactstrap';
function App() {
  const [pedido, setPedido] = useState("")
  const [f1, setF1] = useState("")
  const [f2, setF2] = useState("")
  const [f3, setF3] = useState("")
  const [nopedido, setNoPedido] = useState("")
  const [nopedido2, setNoPedido2] = useState("")

  function handleSubmit(e) {
    e.preventDefault()
     let body = {
          pedido: pedido,
     };
    if (pedido !== "") {
      axios.post("http://localhost:3002/SolicitarPedido", body, {
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((res) => {
          setF1("Su numero de pedido es:" + res.data.id)
          setPedido("")
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }
  
  function handleSubmit1(e) {
  e.preventDefault()  
  let body = {
          id: parseInt(nopedido),
  };
    if (nopedido !== "") {
      axios
        .post("http://localhost:3002/VerificarEstadoRestaurante", body)
        .then((res) => {
          console.log(res.data.Estado)
          setF2("Su pedido esta:" + res.data.Estado)
          setNoPedido("")
          
        })
        .catch((err) => {
          console.log(err);
        });
    }
}
  function handleSubmit2(e) {
  e.preventDefault()
   let body = {
          id: parseInt(nopedido2),
        };
  if (nopedido2 !=="") {
    axios
          .post("http://localhost:3002/VerificarEstadoRepartidor", body)
          .then((res) => {
            setF3("Su pedido esta:" + res.data.Estado)
          setNoPedido2("")
          })
          .catch((err) => {
            console.log(err);
          });
  }
}

  return (
    <Container>
      <Row>
        <Col className="offset-2 mt-4">
      <Form inline onSubmit={handleSubmit}>
        <FormGroup>
          <Label>Solicitar Pedido</Label>
              <Input value={pedido} onChange={e => { setPedido(e.target.value);setF1("")}}/>
        </FormGroup>
        <Button color="primary" >
            Solicitar pedido
          </Button>
          </Form>
          {f1!==""?<UncontrolledAlert color="info">
      {f1}
    </UncontrolledAlert>:<div></div>}
        </Col>
        
      </Row>
      <Row>
        <Col className="offset-2 mt-4">
      <Form inline onSubmit={handleSubmit1}>
        <FormGroup>
          <Label>Verificar pedido con el restaurante</Label>
              <Input value={nopedido} onChange={e => { setNoPedido(e.target.value);setF2("")}}/>
        </FormGroup>
        <Button color="primary" >
            Solicitar pedido
          </Button>
          </Form>
           {f2!==""?<UncontrolledAlert color="info">
      {f2}
    </UncontrolledAlert>:<div></div>}
          </Col>
      </Row>
      <Row>
        <Col className="offset-2 mt-4">
      <Form inline onSubmit={handleSubmit2}>
        <FormGroup>
          <Label>Verificar pedido con el repartidor</Label>
              <Input value={nopedido2} onChange={e => { setNoPedido2(e.target.value); setF3("") }}/>
        </FormGroup>
        <Button color="primary" >
            Solicitar pedido
          </Button>
          </Form>
           {f3!==""?<UncontrolledAlert color="info">
      {f3}
    </UncontrolledAlert>:<div></div>}
          </Col>
      </Row>
    </Container>
  );
}

export default App;
